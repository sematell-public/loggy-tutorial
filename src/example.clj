(ns example)

(in-ns 'example)
(use 'clojure.repl)
(require '[clojure.string :as string])
(set! *print-level* 5)
(set! *print-length* 20)

(+ 1 2)

(+ 1 2 (* 2 3 4))

(inc 1)

(dec 1)

(println "Hello World")                                     ;; are java.lang.String

(str "The " "answer " "is " 42)

(+ 3/10 8/10)

;;; ^^^
;;; Collections & Special forms

(def x 1)
(def x 2)
(def v [1 2 3])
(def s #{1 2 3})
(def m {:a 1, :b 2})
(def l '(1 2 3))

(first v)
(rest v)
(seq m)
(conj v 4)
(conj s 4)
(conj l 4)
(conj m [:c 3])
(assoc m :c 3)


(def t2 (fn [x] (* 2 x)))
(t2 3)

((fn [x] (* 2 x)) 3)

(#(* 2 %) 3)

(defn times-two [x]
  (* x 2)
  )
(times-two 2)

(let [a 1
      b 2]
  (println a " + " b " = " (+ a b))
  )

(defn even->string [x]
  (if (even? x)
    "even"
    "uneven"
    )
  )

(even->string 3)
(even->string 4)


(def enterprise {:class :galaxy
                 :reg "NCC-1701-D"
                 :torpedos 250
                 :commander {:name "Picard", :rank :captain}
                 })

(def voyager {:class :intrepid
              :reg "NCC-74656"
              :torpedos 200
              :commander {:name "Janeway", :rank :captain}
              })

(def defiant {:class :defiant
              :reg "NX-74205"
              :torpedos 100
              :cloak true
              :commander {:name "Worf", :rank :lt-commander}
              })

(def feds [enterprise voyager defiant])

(:reg enterprise)
(assoc enterprise :reg "NCC-1701")
(get-in enterprise [:commander :rank])
(dissoc enterprise :commander)

;;; ^^^
;;; higher-order functions

(map :reg feds)

(defn fire-torpedo [ship]
  (update ship :torpedos dec)
  )

(fire-torpedo enterprise)

(defn fire-torpedos [ship n]
  (update ship :torpedos (fn [old] (- old n)))
  )

(fire-torpedos enterprise 10)

(defn fire-torpedos [ship n]
  (update ship :torpedos #(- % n))
  )

(defn fire-torpedos [ship n]
  (update ship :torpedos - n)
  )

(defn fire-torpedos
  ([ship]
   (fire-torpedos ship 1))
  ([ship n]
   (update ship :torpedos - n)
    )
  )

(defn trigger [ship]
  (fn [n]
    (fire-torpedos ship n)
    )
  )

(def t (trigger enterprise))
(def all-triggers (map trigger feds))
(for [t all-triggers] (t 10))



(filter #(> (:torpedos %) 100) feds)
(map string/lower-case (map :reg feds))

;; Fib

(def fiberator (iterate (fn [fib] [(second fib) (+ (first fib) (second fib))]) [0 1]))

(def fiberator (iterate (fn [[j k]] [k (+ j k)]) [0 1]))

(map first (take 10 fiberator))


;;; Macros

(defn capable-ship? [ship]
  (or (> (:torpedos ship) 200) (:cloak ship))
  )

(capable-ship? defiant)
(capable-ship? voyager)

(macroexpand-1 '(or (> (:torpedos ship) 200) (:cloak ship)))

(defn capable-ship*? [ship]
  (let [exp1 (> (:torpedos ship) 200)]
    (if exp1 exp1 (:cloak ship))
    ))

(defmacro unless [pred a b]
  `(if (not ~pred) ~a ~b))

(unless false (println "Will print") (println "Will not print"))

(->> feds
     (map :reg)
     (map string/lower-case)
     )

(-> {}
    (assoc :a 1)
    (assoc :b 1)
    (update :b inc)
    )

(macroexpand-1 '(-> {}
                   (assoc :a 1)
                   (assoc :b 2)
                   (update :b inc)
                   ))

;;; Atoms

(def battle-group  {
                    :enterprise (merge enterprise {:hp 1000 :torpedo-dmg 3000})
                    :borg       {:reg "Cube 401"
                                 :class :borg
                                 :hp 200000000
                                 :weird-green-laser 1
                                 :laser-dmg 10
                                 }

                    })

(def battle (atom battle-group))

(defn apply-damage [amount ship]
  (update ship :hp - amount)
  )

(defn attack! [ship-id target-id]
  (let [ship (ship-id @battle)
        target (target-id @battle)
        affect (cond
                 (:torpedos ship) (let [ship-after-attack (fire-torpedo ship)
                                        dmg (:torpedo-dmg ship-after-attack)
                                        ]
                                    (swap! battle assoc ship-id ship-after-attack)
                                    (partial apply-damage dmg)
                                    )
                 (:weird-green-laser ship) (let [dmg (:laser-dmg ship)]
                                             (partial apply-damage dmg)
                                             )
                 )]
    (swap! battle assoc target-id (affect target))
    )
  )

(attack! :enterprise :borg)

(attack! :borg :enterprise)

(def attack-order (iterate (fn [order]
                             (if (>= (rand) 0.5)
                               order
                               (reverse order))) [:enterprise :borg]))

(defn do-battle! [n]
  (->> (take n attack-order)
       (map #(apply attack! %))
       ))

(defn delta-hp [old-value new-value]
  (let [dmg-taken
        (map (fn [[k1 ship-old] [k2 ship-new]]
               (- (:hp ship-new) (:hp ship-old)))
             old-value
             new-value
             )
        ]
    (println "Damage:" dmg-taken)
    )
  )

(remove-watch battle :watcher)
(add-watch battle :watcher (fn [_ battle old-value new-value]
                             (delta-hp old-value new-value)
                             ))

;;; Multi-methods

(defmulti who-am-i? class :default :unknown)
(defmethod who-am-i? String [s] (println "am a string!"))
(defmethod who-am-i? Number [s] (println "am a number!"))
(defmethod who-am-i? :unknown [s] (println "don't know!"))

;;; Fire weapons

(defmulti fire-weapons (fn [ship]
                         (or (:class ship) :unknown)))

(defmethod fire-weapons :galaxy [ship]
  (let [ship-after-attack (fire-torpedo ship)
        dmg (:torpedo-dmg ship-after-attack)
        ]
    [(partial apply-damage dmg) ship-after-attack]
    )
  )

(defmethod fire-weapons :borg [ship]
  [(let [dmg (:laser-dmg ship)]
     (partial apply-damage dmg)
     ) ship]
  )

(defn attack*! [ship-id target-id]
  (let [ship (ship-id @battle)
        target (target-id @battle)
        [affect ship-after-attack] (fire-weapons ship)]
    (swap! battle (fn [val]
                    (-> val
                        (assoc target-id (affect target))
                        (assoc ship-id ship-after-attack)
                        )))
    )
  )

(defn do-battle*! [n]
  (->> (take n attack-order)
       (pmap #(apply attack*! %))
       ))


;;; Extend type

(defprotocol WordCount
  (count-words [sentence]))

(extend-type String
  WordCount
  (count-words [s] (count (string/split "hello world" #"\s"))))
;; #"..." is interpreted as regular expression

(count-words "this works!")

;;; Java Interop

(def h (java.util.HashMap.))

;; call method: use . + method name
(.put h "foo" "bar")

;; access field: use .-
(.-x (java.awt.Point. 1 2))

;; static methods or field
(System/getProperty "java.home")

;; implement interface
(def callable (reify java.util.concurrent.Callable
                (call [this] (println "Call me maybe"))))
(.call callable)

(doto (java.util.HashMap.)
      (.put "bubu" "lala")
         ; vs. (.put h "bubu" "lala")
      (.put "foo" "bar"))

;; use .. to chain calls

(.. System (getProperties) (get "java.home"))


;;; Spec

(require '[clojure.spec.alpha :as s])

(s/def ::name string?)

(s/def ::age number?)

(s/def ::person (s/keys :req [::name ::age]))

(s/explain ::person {::name "Picard" ::age 78 })

(s/conform ::name 12)

(s/def ::age (s/and int? (s/or :zero zero? :pos pos?)))

(s/conform ::age 0)

(s/conform ::age 12)

;; Test generation

;;(require '[clojure.spec.gen.alpha :as gen])

;; (def person-generator (s/gen ::person ))

;;(gen/generate person-generator)

;; Unit testing

(require '[clojure.test :as test])

(test/deftest ships
  (test/is (= :galaxy (:class enterprise)))
  )

(test/run-tests)

;; Debugging

#_(defn ranks [ships]
  (let [c (count ships)
        ]
    (sc.api/spy (if (> c 0)
                  (->> ships
                       (map :commander)
                       (map :rank)
                       )
                  ))
    )
  )

