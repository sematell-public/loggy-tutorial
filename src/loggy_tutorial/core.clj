(ns loggy-tutorial.core
  (:require [clojure.string :as str])
  (:import (java.text SimpleDateFormat ParsePosition)))

(defn read-log-file [file]
  (slurp file)
  )

(defn split-lines [log]
  (str/split-lines log)
  )

(def LOG-FORMAT #"([^\[]+)\[(\S+)\] (\S+)\s+(\S+) - (.+)$")
;;(def TS-PARSER (SimpleDateFormat. "yyyy-MM-DD HH:mm:ss,S"))
(def ^ThreadLocal TS-PARSER
  (proxy [ThreadLocal] []
    (initialValue [] (SimpleDateFormat. "yyyy-MM-DD HH:mm:ss,S"))))



(defn parse-timestamps [^SimpleDateFormat parser ^String ts]
  (.parse parser ts (ParsePosition. 0))
  )

(defn extract-info [index log-line]
  (if-let [m (re-find LOG-FORMAT log-line)]
    (let [[_ timestamp thread level class msg] m]
      {:ts     (parse-timestamps (.get TS-PARSER) (str/trim timestamp))
       :thread thread
       :level  (keyword (str/lower-case level))
       :class  class
       :msg    msg
       :line   (inc index)
       })
    log-line
    )
  )

(defn extract-infos [log-lines]
  (map-indexed extract-info log-lines)
  )

(defn update-last
  "Update last element in a vector by running (fn cur val)"
  [coll key fn val]
  (if (not-empty coll)
    (update-in coll [(dec (count coll)) key] fn val)
    coll
    ))

(defn merge-lines [log-lines]
  (reduce (fn [coll val]
            (if (string? val)
              (update-last coll :msg (fn [msg line] (str msg "\n" line)) val)
              (conj coll val)
              )
            ) [] log-lines)
  )

(defn analyze-log-file [file]
  (-> file
      read-log-file
      split-lines
      extract-infos
      merge-lines
      )
  )

(defn log-level? [level log-entry]
  (= level (:level log-entry))
  )

(defn all-warns [log-lines]
  (filter (partial log-level? :warn) log-lines)
  )

(defn msg-contains? [search-string log-lines]
  (filter (comp #(str/includes? % search-string) :msg) log-lines)
  )

(defn contains-string? [field search-string log-lines]
  (filter #(str/includes? (get % field) search-string) log-lines)
  )



;; zum Ausprobieren
(comment
  (def log-lines
    (->> "examples/Config.log"
         read-log-file
         split-lines
         (map extract-info)))
  )
